package Calcolatore;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

public class Client 
{
    public static void main(String[] args) throws Exception
    {
        Socket communication            = new Socket("localhost", 6666);
        ObjectOutputStream request      = new ObjectOutputStream(communication.getOutputStream());
        ObjectInputStream response      = new ObjectInputStream(communication.getInputStream());
        String respLine                 = "";
        
        while (!respLine.equals("bye")) 
        {
            String lineRead             = "";
            BufferedReader stdIn        = new BufferedReader(new InputStreamReader(System.in));
            boolean inputError          = true;
            
            while (inputError) 
            {
                System.out.print("Type in an operation: ");
                lineRead = stdIn.readLine();
                
                if(lineRead.equals("add") || 
                   lineRead.equals("mul") || 
                   lineRead.equals("quit")) 
                    inputError = false;
                else 
                    System.out.println("Unknown operation " + 
                                       lineRead +
                                       ".\nPlease type 'add' for sum or 'mul' for product.");
            }

            Operazione operation = new Operazione(lineRead);
            
            while(!lineRead.toLowerCase().equals("fine") && !lineRead.toLowerCase().equals("quit")) 
            {
                System.out.print("Type in an operand: ");
                lineRead = stdIn.readLine();

                if(!lineRead.toLowerCase().equals("fine")) 
                {
                    try 
                    {
                        operation.AddOperand(Double.parseDouble(lineRead));
                    } 
                    catch (NumberFormatException e) 
                    {
                        System.out.println("Illegal value " + 
                                           lineRead + 
                                           ". Please type a numeric value.");
                    }
                }       
            }

            Operazione respObj;

            if(lineRead.toLowerCase().equals("fine")) 
            {
                request.writeObject(operation);
                respObj = (Operazione) response.readObject();
                
                double result = respObj.getOperands().get(respObj.getOperands().size() - 1);
                System.out.println("The result is: " + result);
            }
            else if (lineRead.toLowerCase().equals("quit")) 
            {
                operation.setOp(lineRead);
                request.writeObject(operation);

                respObj = (Operazione) response.readObject();
                respLine = respObj.getOp();
                
                System.out.println("Server Response: " + respLine.toUpperCase());
            }
        }

        response.close();
        request.close();
        communication.close();
    }
}