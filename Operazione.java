package Calcolatore;
import java.util.*;
public class Operazione implements java.io.Serializable {

    /**
     * Comment for <code>serialVersionUID</code>
     */
    private static final long serialVersionUID = 1994049683158775584L;
    private String op;
    private ArrayList<Double> operands;
    
    public Operazione(String op) 
    {
        super();
        if (!op.toLowerCase().equals("add") && 
            !op.toLowerCase().equals("mul") && 
            !op.toLowerCase().equals("quit"))
            this.op = "";
        else 
            this.op = op.toLowerCase();
        
        this.operands = new ArrayList<Double>();
    }
    
    public Operazione() 
    {
        this("");
    }
    
    public String getOp() 
    {
        return op;
    }
    
    public void setOp(String op) 
    {
        if (op.toLowerCase().equals("add") || 
            op.toLowerCase().equals("mul") || 
            op.toLowerCase().equals("quit") || 
            op.toLowerCase().equals("bye"))
            this.op = op.toLowerCase();
    }

    public ArrayList<Double> getOperands() 
    {
        return operands;
    }

    public void AddOperand(double ops) 
    {
        this.operands.add(ops);
    }
    
    public double calculate() 
    {
        double result = 0.0d;
        Iterator<Double> i = operands.iterator();
        
        if(this.op.equals("add")) 
        {
            while(i.hasNext()) {
                result += i.next();
            }
        } 
        else 
        {
            result = 1.0d;

            while(i.hasNext()) {
                result *= i.next();
            }
        }

        return result;
    }
    
}