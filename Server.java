package Calcolatore;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server 
{
    public static void main(String[] args) throws Exception
    {
        ServerSocket listener = new ServerSocket(6666);
        
        while(true) 
        {
            boolean loop = true;
            Socket communication = listener.accept();
            
            ObjectInputStream request = new ObjectInputStream(communication.getInputStream());
            ObjectOutputStream response = new ObjectOutputStream(communication.getOutputStream()); 
            Operazione op;

            do 
            {
                op = (Operazione) request.readObject();
                
                if(op.getOp().toLowerCase().equals("quit"))
                {
                    op.setOp("bye");
                    loop = false;
                }
                else op.AddOperand(op.calculate());
                
                response.writeObject(op);
            } 
            while (loop);

            response.flush();
            request.close();
            response.close();

            communication.close();
        }
    }
}